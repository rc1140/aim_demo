﻿$(document).ready(function() {
    function playoutItem(initData) {
        var self = this;
        self.artist = initData["@artist"];
        self.duration = initData["@duration"];
        self.imageUrl = initData["@imageUrl"];
        self.status = initData["@status"];
        self.isStatusValid = ko.observable(true);;
        self.time = initData["@time"];
        self.title = initData["@title"];
        self.type = initData["@type"];
        self.imageValid = ko.observable(false);
        self.isPlaying = function() {
            return this.status === 'playing';
        };
        self.validatePlayingStatus = function() {
            if (self.isPlaying()) {
                var timeSinceStart = new Date() - new Date(self.time);
                var minutes = Math.floor(timeSinceStart/ 60000);
                var duration = self.time.split(':');
                var totalMinutes = 0;
                if (parseInt(duration[0]) > 0) {
                    totalMinutes = (parseInt(duration[0]) * 60);
                };
                if (parseInt(duration[1]) > 0) {
                    totalMinutes += parseInt(duration[1]);
                };
                if (parseInt(duration[2]) > 0){
                    totalMinutes += parseInt(duration[1])/60;
                };
                totalMinutes = Math.floor(totalMinutes);

                self.isStatusValid( minutes < totalMinutes);
            };
        }();
        self.validateImageUrl = function() {
            $.ajax({
                type: "HEAD",
                async: true,
                url: self.imageUrl
            }).done(function(message, text, jqXHR) {
                self.imageValid(jqXHR.status === 200);
            });
        }();
    }

    function PlaylistViewModel() {
        var self = this;
        self.playoutItems = ko.observableArray([]);
        self.userURL = ko.observable("http://harry.radioapi.io/services/nowplaying/utv/fm104/onair");
        self.loadUrl = function () {
            $('#loadUrl').text('Loading...');
            $.getJSON("/api/playlist?url=" + this.userURL(), function (allData) {
                var playoutItems = $.map(allData, function (item) { return new playoutItem(item.playoutItem) });
                self.playoutItems(playoutItems);
                $('#loadUrl').text('Load URL');
                console.log(playoutItems);
            });
        };
    }

    ko.applyBindings(new PlaylistViewModel());

});

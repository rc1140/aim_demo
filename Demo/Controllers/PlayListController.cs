﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;

namespace Demo
{
    public class PlaylistController : ApiController
    {
        public IHttpActionResult Get(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return NotFound();
            }
            XDocument playlist = XDocument.Load(url);
            var playoutItems = from onAir in playlist.Descendants("onAir")
                select onAir.DescendantNodes();
            return Ok(playoutItems.FirstOrDefault());
        }
    }
}